Northbound’s rehab treatment in Orange County is located in the heart of beautiful Newport Beach and provides evidence-based therapies and a full continuum of care to help you recover and make long-term addiction recovery from addiction a reality.

Address: 3822 Campus Dr, Newport Beach, CA 92660, USA

Phone: 949-763-3576
